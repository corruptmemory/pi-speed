package com.corruptmemory.pispeed

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, Routers}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector}
import com.typesafe.config.{Config, ConfigFactory}

import java.io.{File, FileInputStream}
import java.math.{BigDecimal, BigInteger, RoundingMode}
import java.util.concurrent.CountDownLatch
import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Main {
  private val TWO = new BigDecimal("2")
  private val FOUR = new BigDecimal("4")
  private val FIVE = new BigDecimal("5")
  private val TWO_THIRTY_NINE = new BigDecimal("239")

  val cpuCount = Runtime.getRuntime.availableProcessors()

  def forkJoinConfig(name:String, parallelismMin:Int = 4, parallelismFactor:Double = 1.0, parallelismMax: Int = cpuCount, throughput:Int = 1):Config = ConfigFactory.parseString(
    s"""$name {
       |  type = Dispatcher
       |  executor = "fork-join-executor"
       |  fork-join-executor {
       |    parallelism-min = $parallelismMin
       |    parallelism-factor = $parallelismFactor
       |    parallelism-max = $parallelismMax
       |  }
       |  throughput = $throughput
       |}
       |""".stripMargin)


  def pi(numDigits: Int): BigDecimal = {
    val calcDigits = numDigits + 10
    FOUR.multiply(FOUR.multiply(arccot(FIVE, calcDigits)).subtract(arccot(TWO_THIRTY_NINE, calcDigits))).setScale(numDigits, RoundingMode.DOWN)
  }

  private def arccot(x: BigDecimal, numDigits: Int) = {
    val unity = BigDecimal.ONE.setScale(numDigits, RoundingMode.DOWN)
    var sum = unity.divide(x, RoundingMode.DOWN)
    var xpower = new BigDecimal(sum.toString)
    var term: BigDecimal = null
    var add = false
    var n = new BigDecimal("3")
    while ( {
      term == null || term.compareTo(BigDecimal.ZERO) != 0
    }) {
      xpower = xpower.divide(x.pow(2), RoundingMode.DOWN)
      term = xpower.divide(n, RoundingMode.DOWN)
      sum = if (add) sum.add(term)
      else sum.subtract(term)
      add = !add

      n = n.add(TWO)
    }
    sum
  }

  private def chunkedSum(in: String, chunkSize: Int): BigInteger = {
    for (_ <- 1 to 100) {
      val chunks = in.replace(".", "").grouped(chunkSize)
      val _ = chunks.foldLeft(BigInteger.ZERO) { case (s, v) =>
        s.add(new BigInteger(v, 10))
      }
    }
    val chunks = in.replace(".", "").grouped(chunkSize)
    chunks.foldLeft(BigInteger.ZERO) { case (s, v) =>
      s.add(new BigInteger(v, 10))
    }
  }

  case class Compute(n: Int, andThen: String => Unit)

  case class Continue(in: String)

  case object Finished

  def time[T](label: String)(body: => T): T = {
    val start = System.currentTimeMillis()
    val r = body
    val end = System.currentTimeMillis()
    println(s"$label: ${end - start}ms")
    r
  }

  private def workAllCompute: Behavior[Compute] = {
    Behaviors.receiveMessage {
      case c: Compute =>
        c.andThen(pi(c.n).toString)
        Behaviors.same
    }
  }

  private def workReadAndCompute(readAmount: Int = 10000): Behavior[Compute] = {
    Behaviors.receiveMessage {
      case c: Compute =>
        val reader = new FileInputStream(new File("/dev/zero"))
        try {
          var rem = readAmount
          val buf = new Array[Byte](100)
          while (rem > 0) {
            reader.read(buf)
            rem -= 1
          }
          val _ = buf
        } catch {
          case e:Throwable =>
            println(s"Uhhh, error! -- $e")
        } finally {
          reader.close()
        }
        c.andThen(pi(c.n).toString)
        Behaviors.same
    }
  }

  private def postProcessAllCompute(sink: ActorRef[Finished.type]): Behavior[Continue] = {
    Behaviors.receiveMessage {
      case c: Continue =>
        val _ = chunkedSum(c.in, 10)
        sink ! Finished
        Behaviors.same
    }
  }


  private def postProcessReadAndCompute(readAmount: Int = 10000)(sink: ActorRef[Finished.type]): Behavior[Continue] = {
    Behaviors.receiveMessage {
      case c: Continue =>
        val reader = new FileInputStream(new File("/dev/zero"))
        try {
          var rem = readAmount
          val buf = new Array[Byte](100)
          while (rem > 0) {
            reader.read(buf)
            rem -= 1
          }
          val _ = buf
        } catch {
          case e:Throwable =>
            println(s"Uhhh, error! -- $e")
        } finally {
          reader.close()
        }
        val _ = chunkedSum(c.in, 10)
        sink ! Finished
        Behaviors.same
    }
  }

  private def devNull(expected: Int, latch: CountDownLatch): Behavior[Finished.type] = {
    var remaining = expected
    Behaviors.receiveMessage {
      case Finished =>
        remaining -= 1
        if (remaining == 0) {
          latch.countDown()
          Behaviors.stopped
        } else Behaviors.same
    }
  }


  private def pipelined(count: Int, work: Behavior[Compute], postProcess: ActorRef[Finished.type] => Behavior[Continue]): Behavior[Unit] = Behaviors.setup[Unit] { ctx: ActorContext[Unit] =>
    val latch = new CountDownLatch(1)
    val dn = ctx.spawn(devNull(count, latch), name = "devNull")
    val cpuCount = Runtime.getRuntime.availableProcessors()
    val ppr = Routers
      .pool(poolSize = cpuCount)(postProcess(dn))
      .withRoundRobinRouting()
      .withRouteeProps(routeeProps =
        DispatcherSelector.fromConfig("post-processor-dispatcher"))
    val pp = ctx.spawn(ppr, "post-processor")
    val wr = Routers
      .pool(poolSize = cpuCount)(work)
      .withRoundRobinRouting()
      .withRouteeProps(routeeProps =
        DispatcherSelector.fromConfig("worker-dispatcher"))
    val w = ctx.spawn(wr, "worker")

    def andThen(in: String): Unit = {
      pp ! Continue(in)
    }

    val m = 25
    for (_ <- 1 to count) {
      w ! Compute(1024 * m, andThen)
    }

    latch.await()

    Behaviors.stopped
  }

  final private def fusedAndThenAllCompute(dn: ActorRef[Finished.type])(in: String): Unit = {
    val _ = chunkedSum(in, 10)
    dn ! Finished
  }

  final private def fusedAndThenReadAndCompute(readAmount: Int = 10000)(dn: ActorRef[Finished.type])(in: String): Unit = {
    val reader = new FileInputStream(new File("/dev/zero"))
    try {
      var rem = readAmount
      val buf = new Array[Byte](100)
      while (rem > 0) {
        reader.read(buf)
        rem -= 1
      }
      val _ = buf
    } catch {
      case e:Throwable =>
        println(s"Uhhh, error! -- $e")
    } finally {
      reader.close()
    }
    val _ = chunkedSum(in, 10)
    dn ! Finished
  }

  private def fused(count: Int, work: Behavior[Compute], andThenIn: ActorRef[Finished.type] => String => Unit): Behavior[Unit] = Behaviors.setup[Unit] { ctx: ActorContext[Unit] =>
    val latch = new CountDownLatch(1)
    val dn = ctx.spawn(devNull(count, latch), name = "devNull")
    val cpuCount = Runtime.getRuntime.availableProcessors()
    val wr = Routers
      .pool(poolSize = cpuCount)(work)
      .withRoundRobinRouting()
      .withRouteeProps(routeeProps =
        DispatcherSelector.fromConfig("worker-dispatcher"))
    val w = ctx.spawn(wr, "worker")

    val andThen = andThenIn(dn)

    val m = 25
    for (_ <- 1 to count) {
      w ! Compute(1024 * m, andThen)
    }

    latch.await()

    Behaviors.stopped
  }


  def runPipeline(count: Int, config: Config, work: Behavior[Compute], postProcess: ActorRef[Finished.type] => Behavior[Continue]): Unit = {
    val system = ActorSystem[Unit](pipelined(count, work, postProcess), "pipelined", config)
    Await.result(system.whenTerminated, Duration.Inf)
  }

  def runFused(count: Int, config: Config, work: Behavior[Compute], andThenIn: ActorRef[Finished.type] => String => Unit): Unit = {
    val system = ActorSystem[Unit](fused(count, work, andThenIn), "fused", config)
    Await.result(system.whenTerminated, Duration.Inf)
  }


  val warmup = 25
  val benchmark = 1200

  def combinedConfigs(configs: Config*):Config = {
    configs.foldLeft(ConfigFactory.load()) { case (s,v) =>
      v.withFallback(s)
    }
  }

  def runFused(work: Behavior[Compute], andThenIn: ActorRef[Finished.type] => String => Unit): Unit = {
    val config: Config = combinedConfigs(forkJoinConfig("worker-dispatcher"), forkJoinConfig("post-processor-dispatcher"))
    println("Warmup fused...")
    runFused(warmup, config, work, andThenIn)

    Thread.sleep(5000)

    println("Running real tests")

    for (i <- 1 to 3) {
      println(s"Run $i:")
      time("fused time")(runFused(benchmark, config, work, andThenIn))
      println("----")
      Thread.sleep(10000)
    }
  }

  def runPipelined(work: Behavior[Compute], postProcess: ActorRef[Finished.type] => Behavior[Continue]): Unit = {
    val config: Config = combinedConfigs(forkJoinConfig("worker-dispatcher"), forkJoinConfig("post-processor-dispatcher"))
    println("Warmup pipelined...")
    runPipeline(warmup, config, work, postProcess)

    Thread.sleep(5000)

    println("Running real tests")

    for (i <- 1 to 3) {
      println(s"Run $i:")
      time("pipelined time")(runPipeline(benchmark, config, work, postProcess))
      println("----")
      Thread.sleep(10000)
    }
  }

  def serialAllCompute(): Unit = {
    val s = pi(1024 * 25).toString
    val _ = chunkedSum(s, 10)
  }

  def serialReadAndCompute(workReadAmount: Int = 10000, postProcessReadAmount:Int = 10000): Unit = {
    def readThingy(readAmount:Int):Unit = {
      val reader = new FileInputStream(new File("/dev/zero"))
      try {
        var rem = readAmount
        val buf = new Array[Byte](100)
        while (rem > 0) {
          reader.read(buf)
          rem -= 1
        }
        val _ = buf
      } catch {
        case e:Throwable =>
          println(s"Uhhh, error! -- $e")
      } finally {
        reader.close()
      }
    }
    readThingy(workReadAmount)
    val s = pi(1024 * 25).toString
    readThingy(postProcessReadAmount)
    val _ = chunkedSum(s, 10)
  }

  def runMaxAllCompute(): Unit = {
    println("Warmup serial...")
    for (_ <- 1 to 10) {
      serialAllCompute()
    }
    Thread.sleep(5000)

    println("Run real benchmark")
    val cpuCount = Runtime.getRuntime.availableProcessors()
    println(s"cpuCount: $cpuCount")
    val a:Int = benchmark / cpuCount
    var b = a
    val slop = (benchmark % cpuCount)
    if (slop != 0) {
      b = a + 1
    }

    def logic(runs: Int, startLatch: CountDownLatch, endLatch: CountDownLatch): Unit = {
      var rem = runs
      startLatch.await()
      while (rem > 0) {
        serialAllCompute()
        rem -= 1
      }
      endLatch.countDown()
    }

    for (i <- 1 to 3) {
      println(s"Run $i:")
      val startLatch = new CountDownLatch(1)
      val endLatch = new CountDownLatch(cpuCount)
      val aRun = new Runnable {
        override def run(): Unit = logic(a, startLatch, endLatch)
      }
      val bRun = new Runnable {
        override def run(): Unit = logic(b, startLatch, endLatch)
      }
      for (_ <- 1 to (cpuCount-slop)) {
        new Thread(aRun).start()
      }
      for (_ <- 1 to slop) {
        new Thread(bRun).start()
      }
      time("theoretical best") {
        startLatch.countDown()
        endLatch.await()
      }
      println("----")
      Thread.sleep(10000)
    }

  }

  def runMaxReadAndCompute(workReadAmount: Int = 10000, postProcessReadAmount:Int = 10000): Unit = {
    println("Warmup serial...")
    for (_ <- 1 to 10) {
      serialReadAndCompute(workReadAmount, postProcessReadAmount)
    }
    Thread.sleep(5000)

    println("Run real benchmark")
    val cpuCount = Runtime.getRuntime.availableProcessors()
    println(s"cpuCount: $cpuCount")
    val a:Int = benchmark / cpuCount
    var b = a
    val slop = (benchmark % cpuCount)
    if (slop != 0) {
      b = a + 1
    }

    def logic(runs: Int, startLatch: CountDownLatch, endLatch: CountDownLatch): Unit = {
      var rem = runs
      startLatch.await()
      while (rem > 0) {
        serialReadAndCompute(workReadAmount, postProcessReadAmount)
        rem -= 1
      }
      endLatch.countDown()
    }

    for (i <- 1 to 3) {
      println(s"Run $i:")
      val startLatch = new CountDownLatch(1)
      val endLatch = new CountDownLatch(cpuCount)
      val aRun = new Runnable {
        override def run(): Unit = logic(a, startLatch, endLatch)
      }
      val bRun = new Runnable {
        override def run(): Unit = logic(b, startLatch, endLatch)
      }
      for (_ <- 1 to (cpuCount-slop)) {
        new Thread(aRun).start()
      }
      for (_ <- 1 to slop) {
        new Thread(bRun).start()
      }
      time("theoretical best") {
        startLatch.countDown()
        endLatch.await()
      }
      println("----")
      Thread.sleep(10000)
    }

  }

  def main(args: Array[String]): Unit = {
    println("*** Theoretical fastest execution - all compute")
    runMaxAllCompute()
    println("*** Theoretical fastest execution - read and compute")
    runMaxReadAndCompute()
    println("\n*** Fused execution - all compute")
    runFused(workAllCompute, fusedAndThenAllCompute)
    println("\n*** Fused execution - read and compute")
    runFused(workReadAndCompute(), fusedAndThenReadAndCompute())
    println("\n*** Pipelined execution - all compute")
    runPipelined(workAllCompute, postProcessAllCompute)
    println("\n*** Pipelined execution - read and compute")
    runPipelined(workReadAndCompute(), postProcessReadAndCompute())
  }
}
