import sbt._

object Dependencies {
  val akkaVersion = "2.6.10"

  val akkaActor = "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion

  def commonDependencies: Seq[ModuleID] =
    Seq(akkaActor)
}
